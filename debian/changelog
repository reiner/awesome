awesome (4.3-7) unstable; urgency=medium

  * Update Standards-Version to 4.6.2.
    - Recalculate wm alternative priority based on Policy.
  * Call dh_installwm only for awesome package.
  * Replace obsolete build and test dependencies:
    libxcb-util0-dev -> libxcb-util-dev.
  * Export XDG_CACHE_HOME during autopkgtest as fontconfig needs it to
    find writable cache directory.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sat, 04 Feb 2023 23:46:48 +0100

awesome (4.3-6) unstable; urgency=medium

  * Build-depend on libgdk-pixbuf-2.0-dev instead of libgdk-pixbuf2.0-dev.
    (Closes: #1019306)
  * Bump Standards-Version to 4.6.1.
  * Bump copyright year.
  * Use api.github.com for searching upstream releases.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 02 Oct 2022 16:31:47 +0200

awesome (4.3-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/tests/control: Add restriction "allow-stderr". (Closes: #1017032)

 -- Marcos Talau <talau@debian.org>  Mon, 05 Sep 2022 13:10:29 -0300

awesome (4.3-5) unstable; urgency=medium

  * Fix FTBFS with GCC 10. (Closes: #957027)
  * Replace obsolete variable ADTTMP with AUTOPKGTEST_TMP in autopkgtest.
  * Bump Standards-Version to 4.5.0.
  * Bump debhelper compat level to 13.
  * Drop explicit nocheck test in dh_override_auto_test, as it is correctly
    skipped in dh compat 13.
  * Update Vcs-* URLs.

 -- Reiner Herrmann <reiner@reiner-h.de>  Wed, 22 Jul 2020 11:58:50 +0200

awesome (4.3-4) unstable; urgency=low

  * Increase timeouts of test runs during autopkgtest.
    Tests are failing on Ubuntu autopkgtest infrastructure on really
    slow hardware. Timeouts are increased to values suggested by upstream.

 -- Reiner Herrmann <reiner@reiner-h.de>  Tue, 26 Feb 2019 18:51:19 +0100

awesome (4.3-3) unstable; urgency=medium

  * Add dependency on gir1.2-gdkpixbuf-2.0 for awesome. (Closes: #920841)
  * Add explicit dependency on gir1.2-glib-2.0 (which was only transitively
    depended on via gir1.2-freedesktop).
  * Recommend gir1.2-gtk-3.0 for gtk theme.

 -- Reiner Herrmann <reiner@reiner-h.de>  Tue, 29 Jan 2019 21:48:12 +0100

awesome (4.3-2) unstable; urgency=medium

  * Create required directories to fix arch:all-only builds.

 -- Reiner Herrmann <reiner@reiner-h.de>  Mon, 28 Jan 2019 22:54:50 +0100

awesome (4.3-1) unstable; urgency=medium

  * New upstream release.
    - handle SIGCHLD directly instead of through GLib (Closes: #886393)
  * Drop obsolete/applied patches and refresh remaining ones.
  * Build-depend on asciidoctor instead of asciidoc. (Closes: #894710)
  * Update Standards-Version to 4.3.0.
    - declare that debian/rules does not require root
  * Bump copyright years.
  * Install upstream release notes into /usr/share/doc/awesome/.
  * Build-depend on debhelper-compat and bump compat level to 12.
  * Update test dependencies.
  * Cherry-pick upstream patches to build example images reproducibly.

 -- Reiner Herrmann <reiner@reiner-h.de>  Mon, 28 Jan 2019 21:57:00 +0100

awesome (4.2-5) unstable; urgency=medium

  * Import upstream patch to fix performance issue with category icon lookup.
    (Closes: #893053)
    - add additional patch for looking up category icons in the correct path
  * Move packaging VCS to salsa.
  * Bump Standards-Version to 4.1.3.
  * Fix spelling in patches.
  * Add check for nocheck in override_dh_auto_test target.
  * Add upstream metadata file.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sat, 17 Mar 2018 19:30:38 +0100

awesome (4.2-4) unstable; urgency=medium

  * Support for freedesktop.org menu. (Closes: #693135)
    - patch default awesomerc.lua to load/use freedesktop module if available
    - recommend awesome-extra, which includes freedesktop module
    - document change in NEWS
  * Fix formatting, spelling and urgency in NEWS.
  * Add libcairo-gobject2 to dependencies. (LP: #1589709)
  * Bump debhelper compatibility/dependency to 11.
  * Bump Standards-Version to 4.1.2.
  * Mention another way of customization in README.Debian. (Closes: #527115)

 -- Reiner Herrmann <reiner@reiner-h.de>  Wed, 27 Dec 2017 15:44:59 +0100

awesome (4.2-3) unstable; urgency=medium

  * Inform about switch to lua 5.3 in NEWS. (Closes: #880143)
  * Bump lua-lgi dependency to 0.9.2; older versions are broken on big endian
    64bit architectures like s390x. (Closes: #879941)

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 05 Nov 2017 12:12:19 +0100

awesome (4.2-2) unstable; urgency=low

  * New maintainer. (Closes: #867316)
  * Build against lua 5.3.
  * Update menu-method generator for lua 5.3.
  * Allow other session bus than dbus-x11. (Closes: #836080)
  * Move API documentation into -doc package. (Closes: #546688)
  * Remove ancient version restrictions.
  * Cherry-pick upstream fix for errors with keyboardlayout widget.
    (Closes: #877757)
  * Document copyright of wallpaper images in default theme.
  * Run unit tests during build.
    - add lua-busted and zsh to build dependencies.
  * Run integration tests as autopkgtest.

 -- Reiner Herrmann <reiner@reiner-h.de>  Mon, 23 Oct 2017 19:20:04 +0200

awesome (4.2-1) unstable; urgency=medium

  * QA upload.

  [ Reiner Herrmann ]
  * New upstream release (Closes: #858170).
  * debian/control:
    - bump Standards-Version to 4.1.1
    - update Homepage field
    - change Vcs-* URLs to https
  * Add upstream signing key.
  * Check upstream signature in watch file.
  * Fix spelling error found by lintian.
  * Register API documentation at doc-base.
  * Amend desktop file with Icon and Keywords entries.
  * Enable all hardening options.
  * Drop build status from documentation to remove privacy breach.
  * Sort table in documentation to build it reproducibly.
  * Honour SOURCE_DATE_EPOCH in generated images in documentation
    of calendar widget.
  * Add myself to debian/copyright.

  [ Julian Wollrath ]
  * Update build dependencies.
  * Bump required lua-lgi version to at least 0.8.0 (Closes: #852969).
  * Drop remove_useless_build_lgi_check.patch.
  * Refresh install-and-debian-menu.
  * Provide notification-daemon. (Closes: #734645)
  * Do not install README.md link. (Closes: #856442)

  [ Nicolas Boulenguez ]
  * Orphan the package (see #867316).
  * Run wrap-and-sort.
  * Stop overriding dh_installwm, priority 20 is the default.
  * Let watch file track github tags.
  * Switch to debhelper 10.
  * Switch to copyright format 1.0. Order like docs/00-authors.md.
  * Generate menu icon during build.

  [ Łukasz Stelmach ]
  * Add support for starting other window managers in Debian menu.
    (Closes: #856476)

 -- Reiner Herrmann <reiner@reiner-h.de>  Mon, 02 Oct 2017 23:57:00 +0200

awesome (4.0-1) unstable; urgency=medium

  * New upstream release. Closes: #850701, #851791.
    + d/patches:
      - Refreshed.
      - Drop Prevent_clients_from_being_lost_on_RANRD_changes.patch.
    + d/control:
      - Add B-D against libxcb-xrm-dev, libxcb-xkb-dev,
        libxkbcommon-x11-dev and libxkbcommon-dev.
      - Add B-D against lua-lgi (needed for docs/*.md).
    + d/docs: README renamed to README.md.
    + d/rules: Disable integration tests (currently fails).

 -- Arnaud Fontaine <arnau@debian.org>  Thu, 19 Jan 2017 15:25:32 +0900

awesome (3.5.9-1) unstable; urgency=medium

  * New upstream release. Closes: #817122.
  * d/control: Bump Standards-Versions to 3.9.7. No change needed.
  * dbgsym are now built automatically (debhelper > 9.20151219).
    Closes: #532383.

 -- Arnaud Fontaine <arnau@debian.org>  Sun, 27 Mar 2016 11:20:44 +0900

awesome (3.5.6-1) unstable; urgency=medium

  * New upstream release and upload to unstable.
    Closes: #781736, #680390, #701514, #589226.
  * Add debian/patches/Prevent_clients_from_being_lost_on_RANRD_changes.patch.
    Closes: #736314.
  * debian/55awesome-javaworkaround: Fix typo (x-session-manager ->
    x-window-manager). Thanks to Igor Bensemann and Julian Wollrath.
    Closes: #600995.
  * d/control:
    + Bump Standards-Version to 3.9.6. No change needed.
    + Drop Build-Depends on libxcb-image0-dev since #732711 has been fixed.

 -- Arnaud Fontaine <arnau@debian.org>  Fri, 05 Jun 2015 13:44:11 +0900

awesome (3.5.5-1) experimental; urgency=low

  * New upstream release. Closes: #743507.
    + debian/control: Bump lua-lgi Depends to >= 0.7.0.

 -- Arnaud Fontaine <arnau@debian.org>  Wed, 11 Jun 2014 11:14:11 +0900

awesome (3.5.2-1) experimental; urgency=low

  * New upstream release. Closes: #731097.
    + debian/control:
      - libxcb-image0-dev B-D is not needed anymore as cairo is now used
        instead, but keep it until #732711 is fixed.
      - Drop libxcursor-dev and libx11-xcb-dev B-D replaced by
        libxcb-cursor-dev >= 0.1.0-2 because of #723750.
  * debian/control:
    + Bump Standards-Version to 3.9.5. No changes needed.
    + Since 3.5, glib is used instead of libev, but libglib2.0-dev B-D was
      pulled implicitly by another B-D and now seems to be required.
    + Build-Depends on lua-ldoc as it is now available in the archive.

 -- Arnaud Fontaine <arnau@debian.org>  Fri, 20 Dec 2013 15:57:46 +0100

awesome (3.5.1-1) experimental; urgency=low

  * New upstream release. Closes: #697428.
    + debian/NEWS: Inform users that the configuration has changed.
    + debian/control:
      - Drop luadoc, libpango1.0-dev, libev-dev, libimlib2-dev, and gperf.
      - Add libgdk-pixbuf2.0-dev in Build-Depends.
      - Add lua-lgi, gir1.2-freedesktop and gir1.2-pango-1.0 to Depends.
        - Disable build time check of .typelib as it is used only at runtime.
    + debian/copyright: update.
    + debian/patches/*: refresh.
  * debian/control:
    + Bump Standards-Version to 3.9.4. No changes needed.
    + Update Vcs-Git and Vcs-Browser as it is now in collab-maint.
  * Add myself as Uploaders, acknowledged by Julien Danjou.

 -- Arnaud Fontaine <arnau@debian.org>  Tue, 04 Jun 2013 18:48:21 +0900

awesome (3.4.15-1) unstable; urgency=low

  * New upstream release (Closes: #700607)

 -- Julien Danjou <acid@debian.org>  Sun, 17 Feb 2013 10:44:54 +0100

awesome (3.4.14-1) unstable; urgency=low

  * New uptream release

 -- Julien Danjou <acid@debian.org>  Tue, 25 Dec 2012 18:58:38 +0100

awesome (3.4.13-1) unstable; urgency=low

  * Enhance description in debian/control (Closes: #680924)
    (Thanks to Justin B Rye)
  * New upstream release
    + Fix focus regression (Closes: #681364)

 -- Julien Danjou <acid@debian.org>  Sun, 15 Jul 2012 15:51:10 +0200

awesome (3.4.12-2) unstable; urgency=low

  * Don't build depends on libxcb-ewmh-dev

 -- Julien Danjou <acid@debian.org>  Wed, 13 Jun 2012 10:56:53 +0200

awesome (3.4.12-1) unstable; urgency=low

  * New upstream release.
    + Fix GLib compilation (Closes: #665508)
    + Fix prompt history order (Closes: #661669)
    + Xli is no more marked as broken (Closes: #651505)
  * Bump standard version
  * Tweak debian/rules to specify cmake build system, update to dh 9

 -- Julien Danjou <acid@debian.org>  Mon, 11 Jun 2012 15:05:38 +0200

awesome (3.4.11-2) unstable; urgency=low

  * Add feh in Recommends (Closes: #658647)

 -- Julien Danjou <acid@debian.org>  Mon, 06 Feb 2012 10:03:14 +0100

awesome (3.4.11-1) unstable; urgency=low

  * New upstream release.
  * Drop xcb 0.3.8 patches (now upstream has it)
  * Bump standard version

 -- Julien Danjou <acid@debian.org>  Wed, 23 Nov 2011 15:33:07 +0100

awesome (3.4.10-1) unstable; urgency=low

  * New upstream release.

 -- Julien Danjou <acid@debian.org>  Wed, 29 Jun 2011 14:51:02 +0200

awesome (3.4.9-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Cherry-pick patch from upstream to deal with xcb-util 0.3.8.
  * Update build-deps accordingly.

 -- Julien Cristau <jcristau@debian.org>  Sat, 28 May 2011 23:53:51 +0200

awesome (3.4.9-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Patch CMakeList to build successfully. (Closes: #614531)

 -- Ming-Ting Yao Wei <medicalwei@gmail.com>  Sat, 30 Apr 2011 13:56:39 +0800

awesome (3.4.9-1) unstable; urgency=low

  * New upstream release
  * Bump standard version

 -- Julien Danjou <acid@debian.org>  Mon, 17 Jan 2011 14:46:26 +0100

awesome (3.4.8-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Mon, 04 Oct 2010 11:12:23 +0200

awesome (3.4.7+git-4-g92183ca-1) unstable; urgency=low

  * New upstream snapshot
    + Fix systray size computing (Closes: #552207)

 -- Julien Danjou <acid@debian.org>  Tue, 31 Aug 2010 21:43:47 +0200

awesome (3.4.6-1) unstable; urgency=low

  * New upstream release
  * Bump standards policy

 -- Julien Danjou <acid@debian.org>  Wed, 14 Jul 2010 08:49:49 +0200

awesome (3.4.5-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Thu, 13 May 2010 08:57:22 +0200

awesome (3.4.4-1) unstable; urgency=low

  * New upstream release
  * Workaround Java bug (Closes: #573097)
  * Bump standards policy

 -- Julien Danjou <acid@debian.org>  Tue, 09 Mar 2010 12:04:57 +0100

awesome (3.4.3-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Tue, 19 Jan 2010 14:02:18 +0100

awesome (3.4.2-3) unstable; urgency=low

  * Restore wm alternative (Closes: #560150)

 -- Julien Danjou <acid@debian.org>  Thu, 10 Dec 2009 11:17:36 +0100

awesome (3.4.2-2) unstable; urgency=low

  * Switch to dh7 and 3.0 source format

 -- Julien Danjou <acid@debian.org>  Fri, 27 Nov 2009 11:12:08 +0100

awesome (3.4.2-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Thu, 26 Nov 2009 20:12:24 +0100

awesome (3.4.1-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Mon, 09 Nov 2009 16:16:06 +0100

awesome (3.4-1) unstable; urgency=low

  * New upstream release
    + Works without rlwrap (Closes: #546466)
    + Default bindings for tags use keycode (Closes: #508417)
    + Switch back to the old unmap banning code (Closes: #528512)

 -- Julien Danjou <acid@debian.org>  Tue, 20 Oct 2009 21:02:30 +0200

awesome (3.4~rc3-1) experimental; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Fri, 09 Oct 2009 16:23:31 +0200

awesome (3.4~rc2-1) experimental; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Mon, 28 Sep 2009 14:25:12 +0200

awesome (3.4~rc1-1) experimental; urgency=low

  * New upstream release
    + Fixes bashisms in awesome-client (Closes: #541726)
    + Fix problems with timers (Closes: #533749)

 -- Julien Danjou <acid@debian.org>  Fri, 11 Sep 2009 14:12:31 +0200

awesome (3.3.4-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Mon, 07 Sep 2009 12:47:25 +0200

awesome (3.3.3-2) unstable; urgency=low

  * Cherry-pick WM_TRANIENT_FOR fix from 3.3 branch (Closes: #543481)

 -- Julien Danjou <acid@debian.org>  Fri, 28 Aug 2009 16:47:43 +0200

awesome (3.3.3-1) unstable; urgency=low

  * New upstream release
    + Fix WM_TRANSIENT_FOR loop (Closes: #543481, #529025)
  * Bump standard version

 -- Julien Danjou <acid@debian.org>  Tue, 25 Aug 2009 14:07:50 +0200

awesome (3.3.2-1) unstable; urgency=low

  * New upstream release
    + Do not exit when D-Bus connection is closed (Closes: #527872)
  * Bump standard version
  * Depends on dbus-x11 (Closes: #534272)

 -- Julien Danjou <acid@debian.org>  Mon, 27 Jul 2009 15:29:53 +0200

awesome (3.3.1-1) unstable; urgency=low

  * New upstream release
    + Do not exit when D-Bus connection is closed (Closes: #527872)

 -- Julien Danjou <acid@debian.org>  Thu, 18 Jun 2009 16:25:05 +0200

awesome (3.3-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Thu, 04 Jun 2009 12:00:07 +0200

awesome (3.3~rc4-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Tue, 26 May 2009 17:00:13 +0200

awesome (3.3~rc3-1) unstable; urgency=low

  * New upstream release
  * Build-depends on ImageMagick to build titlebar icons

 -- Julien Danjou <acid@debian.org>  Mon, 18 May 2009 11:28:00 +0200

awesome (3.3~rc2-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Fri, 08 May 2009 15:27:50 +0200

awesome (3.3~rc1-1) unstable; urgency=low

  * New upstream release
    + Build against new libxcb-keysyms (Closes: #525506)
    + Fullscreen layout has an icon (Closes: #521393)
    + New spawn code based on glib (Closes: #501291)
    + New spawn code return errors in prompt (Closes: #502327)
  * Recommends rlwrap for awesome-client
  * Remove auto-generated menu upon purge (Closes: #524546)

 -- Julien Danjou <acid@debian.org>  Fri, 01 May 2009 18:15:45 +0200

awesome (3.2.1-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Sat, 04 Apr 2009 17:20:54 +0200

awesome (3.2-1) unstable; urgency=low

  * New upstream release
    + Fix a bug with handling of some D-Bus type (Closes: #516383)
    + Print regular time now (Closes: #508416)
  * Bump standard policy

 -- Julien Danjou <acid@debian.org>  Fri, 13 Mar 2009 11:09:36 +0100

awesome (3.2~rc4-1) unstable; urgency=low

  * New upstream release
  * Add libdbus build-dep.

 -- Julien Danjou <acid@debian.org>  Thu, 26 Feb 2009 17:47:41 +0100

awesome (3.2~rc3-1) unstable; urgency=low

  * New upstream release.

 -- Julien Danjou <acid@debian.org>  Fri, 20 Feb 2009 15:40:31 +0100

awesome (3.2~rc1-2) experimental; urgency=low

  * Add missing build-deps on libxcb-image0-dev
  * Remove useless x11-xcb build-dep

 -- Julien Danjou <acid@debian.org>  Tue, 10 Feb 2009 17:20:11 +0100

awesome (3.2~rc1-1) experimental; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Fri, 06 Feb 2009 18:06:27 +0100

awesome (3.1.2-1) experimental; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Thu, 05 Feb 2009 15:49:47 +0100

awesome (3.1-1) experimental; urgency=low

  * New upstream release
  * Add a versioned build dependency on cmake (Closes: #508344)

 -- Julien Danjou <acid@debian.org>  Wed, 10 Dec 2008 13:36:35 +0100

awesome (3.1~rc5-1) experimental; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Fri, 05 Dec 2008 10:54:15 +0100

awesome (3.1~rc4-1) experimental; urgency=low

  * New upstream version
    + Fix naughty (Closes: #506803, #506605)
  * Use editor rather than nano in default config (Closes: #507023)

 -- Julien Danjou <acid@debian.org>  Fri, 28 Nov 2008 14:53:49 +0100

awesome (3.1~rc3-1) experimental; urgency=low

  * New upstream version

 -- Julien Danjou <acid@debian.org>  Fri, 21 Nov 2008 11:24:37 +0100

awesome (3.1~rc2-3) experimental; urgency=low

  * Add menu as a dependency

 -- Julien Danjou <acid@debian.org>  Tue, 18 Nov 2008 11:22:29 +0100

awesome (3.1~rc2-2) experimental; urgency=low

  * Fix typo in default configuration file (Closes: #505973)

 -- Julien Danjou <acid@debian.org>  Mon, 17 Nov 2008 11:52:12 +0100

awesome (3.1~rc2-1) experimental; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Fri, 14 Nov 2008 18:12:03 +0100

awesome (3.1~rc1-4) experimental; urgency=low

  * Fix debian menu generation

 -- Julien Danjou <acid@debian.org>  Wed, 12 Nov 2008 10:52:01 +0100

awesome (3.1~rc1-3) experimental; urgency=low

  * Add x11-xcb and imlib2 as build-deps (Closes: #504978)

 -- Julien Danjou <acid@debian.org>  Sat, 08 Nov 2008 12:12:59 +0100

awesome (3.1~rc1-2) experimental; urgency=low

  * Add Debian menu via menu-method
  * Cherry-pick:
    luaa: add XDG_CONFIG_DIR as include path

 -- Julien Danjou <acid@debian.org>  Fri, 07 Nov 2008 16:43:30 +0100

awesome (3.1~rc1-1) experimental; urgency=low

  * New upstream release
    + Floating placement is now on Lua side and documented (Closes: #499979)
    + Square size is reduced (Closes: #499975)
  * Specify luadoc version in build-dependencies (Closes: #501045)
  * Add Vcs field in debian/control (Closes: #504289)

 -- Julien Danjou <acid@debian.org>  Sun, 02 Nov 2008 15:45:56 +0100

awesome (3.0-1) experimental; urgency=low

  * New upstream release
  * Update build-deps to new xcb-util

 -- Julien Danjou <acid@debian.org>  Tue, 23 Sep 2008 15:53:42 +0200

awesome (3.0~rc6-1) experimental; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Fri, 05 Sep 2008 01:17:01 +0200

awesome (3.0~rc5-1) experimental; urgency=low

  * New upstream release
    + Fix background color on systray icons (Closes: #493760)
  * Does not depend anymore on liblua5.1-filesystem

 -- Julien Danjou <acid@debian.org>  Fri, 29 Aug 2008 11:50:36 +0200

awesome (3.0~rc4-1) experimental; urgency=low

  * New upstream release
    + Depends on liblua5.1-filesystem

 -- Julien Danjou <acid@debian.org>  Fri, 22 Aug 2008 13:11:33 +0200

awesome (3.0~rc3-1) experimental; urgency=low

  * New upstream release
    + Fix systray display (Closes: #494986)
    + Do no wake-up every second anymore if not needed (Closes: #493767)

 -- Julien Danjou <acid@debian.org>  Fri, 15 Aug 2008 12:21:36 +0200

awesome (3.0~rc2-1) experimental; urgency=low

  * New upstream release
  * Build-depends on lua5.1 rather on lua

 -- Julien Danjou <acid@debian.org>  Mon, 11 Aug 2008 13:39:36 +0200

awesome (3.0~rc1-2) experimental; urgency=low

  * Add versioned build-dep on xcb-util (Closes: #493929)

 -- Julien Danjou <acid@debian.org>  Wed, 06 Aug 2008 17:49:52 +0200

awesome (3.0~rc1-1) experimental; urgency=low

  * New upstream release
    + Fix titlebar show in maximised (Closes: #491142)
    + Initialize font by default (Closes: #474935)

 -- Julien Danjou <acid@debian.org>  Sat, 02 Aug 2008 18:27:33 +0200

awesome (2.3.4-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Sun, 24 Aug 2008 15:10:59 +0200

awesome (2.3.3-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Sat, 26 Jul 2008 12:06:46 +0200

awesome (2.3.2-1) unstable; urgency=low

  * New upstream release
  * Bump standard version

 -- Julien Danjou <acid@debian.org>  Tue, 24 Jun 2008 09:41:03 +0200

awesome (2.3.1-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Mon, 02 Jun 2008 19:32:09 +0200

awesome (2.3-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Wed, 07 May 2008 09:44:07 +0200

awesome (2.3~rc3-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Mon, 21 Apr 2008 12:13:13 +0200

awesome (2.3~rc2-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Thu, 17 Apr 2008 20:40:17 +0200

awesome (2.3~rc1-1) unstable; urgency=low

  * New upstream release
  * Stop shipping awesomerc.gz twice (Closes: #472521)

 -- Julien Danjou <acid@debian.org>  Mon, 07 Apr 2008 08:39:39 +0200

awesome (2.2-1) unstable; urgency=low

  * New upstream release
  * Improve description

 -- Julien Danjou <acid@debian.org>  Sun, 23 Mar 2008 21:14:23 +0100

awesome (2.2~rc4-1) unstable; urgency=low

  * New upstream release
    + Fix problem with stacking windows (Closes: #468860)

 -- Julien Danjou <acid@debian.org>  Thu, 13 Mar 2008 11:07:58 +0100

awesome (2.2~rc3-1) unstable; urgency=low

  * New upstream release
    + Fix focus problems with EnterWindow events (Closes: #467385)

 -- Julien Danjou <acid@debian.org>  Tue, 04 Mar 2008 15:20:22 +0100

awesome (2.2~rc2-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Mon, 25 Feb 2008 17:33:27 +0100

awesome (2.2~rc1-1) unstable; urgency=low

  * New upstream release
    + Fix maximized layout (Closes: #463506)
    + Fix problem with zoom (Closes: #464095)

 -- Julien Danjou <acid@debian.org>  Thu, 14 Feb 2008 14:11:01 +0100

awesome (2.1-1) unstable; urgency=low

  * New upstream release
    + Document new layouts (Closes: #460653)

 -- Julien Danjou <acid@debian.org>  Mon, 21 Jan 2008 16:21:29 +0100

awesome (2.1~rc2-1) unstable; urgency=low

  * New upstream release
    + Fix segmentation fault with statusbar (Closes: #459992)
    + Fix slowness with Audacious (Closes: #457814)

 -- Julien Danjou <acid@debian.org>  Sat, 12 Jan 2008 14:25:40 +0100

awesome (2.1~rc1-1) unstable; urgency=low

  * New upstream release
    + Respect ratio hints for floating windows (Closes: #455844)
    + Fix manpage about reloading (Closes: #455846)
    + No more die on bad config (Closes: #455851)
    + Fix border with mplayer (Closes: #457813)
    + No more default awesomerc (Closes: #457850)
    + awesomerc not match internal default (Closes: #455847)
  * Add Mod4 information and how to read man in README.Debian (Closes: #455989)
  * Changes default colors to be more Debianish (Closes: #455988)
  * Use x-terminal-emulator instead of xterm (Closes: #455987)

 -- Julien Danjou <acid@debian.org>  Wed, 09 Jan 2008 19:10:53 +0100

awesome (2.0.final-1) unstable; urgency=low

  * New upstream release
    + awesome can run without configuration file (Closes: #454551)
  * Bump standards version

 -- Julien Danjou <acid@debian.org>  Tue, 11 Dec 2007 14:48:29 +0100

awesome (2.0-rc2-3) unstable; urgency=low

  * Add icon
  * Add desktop entry (Closes: #453489)
  * Call dh_installwm to use alternative

 -- Julien Danjou <acid@debian.org>  Sun, 02 Dec 2007 15:49:38 +0100

awesome (2.0-rc2-2) unstable; urgency=low

  * Add asciidoc as build-dep

 -- Julien Danjou <acid@debian.org>  Thu, 29 Nov 2007 20:29:06 +0100

awesome (2.0-rc2-1) unstable; urgency=low

  * New upstream release

 -- Julien Danjou <acid@debian.org>  Thu, 29 Nov 2007 18:32:16 +0100

awesome (2.0-rc1-1) unstable; urgency=low

  * Initial release (Closes: #445377)

 -- Julien Danjou <acid@debian.org>  Fri, 16 Nov 2007 20:00:23 +0100
